from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
import pandas as pd
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import StratifiedShuffleSplit, GridSearchCV
from sklearn.feature_extraction.text import CountVectorizer
from nltk.tokenize import TweetTokenizer
from sklearn.svm import SVC
from nltk.stem.porter import PorterStemmer
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
from yellowbrick.text import FreqDistVisualizer


#fiserul csv cu datele de antrenare si test
filename = 'twitter_data.csv'

#utility functions

def get_data(file):

    tw_tokenizer = TweetTokenizer(strip_handles=True, reduce_len=True, )
    stemmer = PorterStemmer()

    df = pd.read_csv(file, delimiter=',')

    tweets = df['tweet'].values
    labels = df['label'].values
    negative_sentences = []  #folosit pt analiza pe hatred speech

    #applying stemming and wordtokenizer
    for index in range(len(tweets)):
        tokens = tw_tokenizer.tokenize(tweets[index])
        stemmed_tokens = [stemmer.stem(token) for token in tokens if token.isalnum()]  #se elimina si caracterele care nu sunt alfanumerice
        tweets[index] = ' '
        tweets[index] = tweets[index].join(stemmed_tokens)      #reconstructia propozitiei dupa ce textul a fost curatat
        if(labels[index]) == 1:
            negative_sentences.append(tweets[index])


    # better splitting, train and test sets have approx same % of samples of each target as complete set
    shuffle_stratified = StratifiedShuffleSplit(n_splits=1, test_size=0.3)

    for train_index, test_index in shuffle_stratified.split(tweets, labels):
        tweet_train = tweets[train_index]
        tweet_test = tweets[test_index]
        labels_train = labels[train_index]
        labels_test = labels[test_index]

    return tweet_train, labels_train, tweet_test, labels_test, negative_sentences

def top_words(data, top_number):

    vec = CountVectorizer(lowercase=True, analyzer='word', stop_words='english')
    bag_of_words = vec.fit_transform(data)
    sum_words = bag_of_words.sum(axis=0) #aduna aparitia fiecarui cuvant din corpus, pe coloane (axis=0)

    #creaza o lista de tupluri (cuvant, frecventa_cuvant) si o sorteaza in ordinea frecventei
    words_freq = []
    words_freq = [(word, sum_words[0, index]) for word, index in vec.vocabulary_.items()]
    words_freq = sorted(words_freq, key = lambda x: x[1], reverse=True)

    #creaza un dicitonar al top n cuvinte (n dat ca parametru), care va fi ordonat descrescator
    top_words = {}
    for index in range(top_number):
        top_words[words_freq[index][0]] = words_freq[index][1]

    return top_words

def wordCloud(data, backgroundcolor='black', width=800, height=600):
    wordcloud = WordCloud(stopwords=STOPWORDS, background_color=backgroundcolor,
                          width=width, height=height).generate_from_frequencies(data)
    plt.figure(figsize=(10, 6))
    plt.imshow(wordcloud)
    plt.axis("off")
    plt.show()

def frequency_distribution_graph(data):

    # vec = CountVectorizer(lowercase=True, analyzer='word', stop_words='english')
    vec = CountVectorizer(lowercase=True, analyzer='word')
    bag_of_words = vec.fit_transform(data)
    sum_words = bag_of_words.sum(axis=0)

    features = vec.get_feature_names()
    visualizer = FreqDistVisualizer(features=features, n=20, orient='h')
    visualizer.fit(bag_of_words)
    visualizer.show()

def grid_search(x_train, y_train):
    param_grid = [
        {'C': [0.01, 1, 10, 100, 1000], 'class_weight': [None, 'balanced']}
    ]

    grid_search = GridSearchCV(SVC(), param_grid, scoring='recall', cv=5, n_jobs=-1)  # n_jobs setat sa foloseasca toate core-urile
    grid_search.fit(x_train, y_train)
    print("Best parameters: ")
    print(grid_search.best_params_)
    return grid_search.best_params_

#classifier types

def svn_model_balanced_report(X_train, labels_train, X_test, labels_test):
    print("----------------------------")
    print("SVM MODEL BALANCED")
    model = SVC(kernel='linear', class_weight='balanced', C=1)
    model.fit(X_train, labels_train)
    predictions = model.predict(X_test)
    print(classification_report(labels_test, predictions))

def svn_model_unbalanced_report(X_train, labels_train, X_test, labels_test):
    print("----------------------------")
    print("SVM MODEL UNBALANCED")
    model = SVC(kernel='linear', C=1)
    model.fit(X_train, labels_train)
    predictions = model.predict(X_test)
    print(classification_report(labels_test, predictions))

def multonomial_naive_bayes_report(X_train, labels_train, X_test, labels_test):
    print("----------------------------")
    print("MULTINOMIAL NAIVE BAYES MODEL")

    model = MultinomialNB()  # try with alpha=0.01  ##hyperparameter
    model.fit(X_train, labels_train)

    predictions = model.predict(X_test)

    print(accuracy_score(labels_test, predictions))
    print(classification_report(labels_test, predictions))
    print(model.predict_proba(X_test[0]))

def logistic_regression_model(X_train, labels_train, X_test, labels_test):
    print("----------------------------")
    print("LOGISTIC REGRESSION MODEL")

    model = LogisticRegression()  # try with alpha=0.01
    model.fit(X_train, labels_train)

    predictions = model.predict(X_test)

    print(accuracy_score(labels_test, predictions))
    print(classification_report(labels_test, predictions))
    print(model.predict_proba(X_test[0]))



tweet_train, labels_train, tweet_test, labels_test, negative_sentences = get_data(filename)

count_vectorizer = CountVectorizer(lowercase=True, analyzer='word', stop_words='english')
count_vectorizer.fit(tweet_train)
X_train = count_vectorizer.transform(tweet_train)
X_test = count_vectorizer.transform(tweet_test)


#raport cu tipuri multiple de clasificatori:
# svn_model_balanced_report(X_train, labels_train, X_test, labels_test)
# svn_model_unbalanced_report(X_train, labels_train, X_test, labels_test)
# multonomial_naive_bayes_report(X_train, labels_train, X_test, labels_test)
# logistic_regression_model(X_train, labels_train, X_test, labels_test)


#top 50 cuvinte in setul de antrenare
top_words_training = top_words(tweet_train, 25)
#top 50 cuvinte in propozitiile etichetate ca hatred speech
top_negative_words = top_words(negative_sentences, 25)


#bar chart cu top 20 cuvinte in propozittile negative in functie de frecventa lor
frequency_distribution_graph(negative_sentences)


#estimeaza cei mai buni parametri pentru clasificator SVM
#rezultate pe setul original: C=1, class_weight=balanced

#ruleaza incet, comentat ca sa nu incetineasca programul
#grid_search(X_train, labels_train)


# afiseaza un wordcloud cu cele mai populare 25 de cuvinte din propozitiile negative
wordCloud(top_negative_words)

# afiseaza un wordcloud cu cele mai populare 25 de cuvinte din setul de antrenare
wordCloud(top_words_training)



multonomial_naive_bayes_report(X_train, labels_train, X_test, labels_test)

logistic_regression_model(X_train, labels_train, X_test, labels_test)

svn_model_balanced_report(X_train, labels_train, X_test, labels_test)
